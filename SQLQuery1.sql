create database test;
GO
use test;
GO
create table Department
(
Id int primary key Identity(1, 1),
[Name] nvarchar(100) NOT NULL,
);
GO
create table Employee
(
Id int primary key Identity(1, 1),
Chief_Id int NULL references Employee(Id),
[Name] nvarchar(100) NOT NULL,
Salary money NOT NULL default (100)
);
GO
alter table Employee add Department_Id int NOT NULL;
GO
alter table Employee add foreign key (Department_Id) references Department(Id);
GO
insert into Department (Name) values ('Technology');
insert into Department (Name) values ('Accountants');
insert into Department (Name) values ('Cleaning');
GO
select Concat(Id, ' Id' ) as [IDs], [Name] as [DepartmentName] from Department;
select Id, [Name] from Department;
GO
delete from Department where Id > 4;
GO
insert into Employee (Chief_Id, [Name], Salary, Department_Id) values (NULL, 'Vasya', 10000, 1);
GO
update Employee set [Name] = 'Boss' where [Name] = 'Vasya';
GO
select SCOPE_IDENTITY(); ��������� ����������� ��������
GO
insert into Employee (Chief_Id, [Name], Salary, Department_Id) values (SCOPE_IDENTITY(), 'Vasya', 100, 1);
insert into Employee (Chief_Id, [Name], Salary, Department_Id) values (2, 'Petya', 200, 1);
insert into Employee (Chief_Id, [Name], Salary, Department_Id) values (2, 'Katya', 300, 1);
insert into Employee (Chief_Id, [Name], Salary, Department_Id) values (1, 'Dima', 400, 2);
insert into Employee (Chief_Id, [Name], Salary, Department_Id) values (5, 'Olya', 500, 2);
insert into Employee (Chief_Id, [Name], Salary, Department_Id) values (5, 'Valya', 600, 2);
insert into Employee ([Name], Salary, Department_Id) values ('Misha', 400, 1);
insert into Employee ([Name], Salary, Department_Id) values ('Grisha', 700, 2);
GO
select * from Employee where Chief_Id IS NOT NULL;
select * from Employee where Chief_Id IS NULL;
select * from Employee where Salary < 1000;

----1. ��������������� � �������� ������� ������ ������� � ����������� �����������
SELECT d.Name, COUNT(emp.Id) as EmpCount
FROM Department d
LEFT JOIN Employee emp on emp.Department_Id = d.Id
group by d.Name
ORDER by EmpCount DESC;
----2. ������� ������ �����������, ���������� ���������� ����� ������� ��� � ����������������� ������������
select emp.Name, emp.Salary
from Employee emp
join Employee men on emp.Chief_Id = men.Id
where emp.Salary > men.Salary;
----3. ������� ������ �������, ���������� ����������� � ������� �� ��������� 3 �������
select Department.Name 
from Department
left join Employee on Department.Id = Employee.Department_Id
group by Department.Name
having count(Employee.Id) <= 3;
----4. ������� ������ �����������, ���������� ������������ ���������� ����� � ����� ������
select Department.Name, emp.Name, emp.Salary
from Employee emp
join Department on Department.Id = emp.Department_Id
where emp.Salary = any
(select MAX(Salary) as Salary
from Employee 
join Department on Department.Id = Employee.Department_Id
group by Department.Name);
----5. ����� ������ ������� � ������������ ��������� ��������� �����������
SELECT d.Name, SUM(e.Salary) as SalarySum
FROM Department d 
JOIN Employee e ON d.Id = e.Department_Id 
GROUP BY d.Name
HAVING SUM(e.Salary) = 
(SELECT MAX(s.SalarySum) AS MaxSalary
FROM (SELECT d.Name, SUM(e.Salary) as SalarySum
FROM Department d 
JOIN Employee e ON d.Id = e.Department_Id 
GROUP BY d.Name) s);
----6. ������� ������ �����������, �� ������� ������������ ������������, ����������� � ���-�� ������
select dep.Name, emp.Name 
from Employee emp
join Department dep on dep.Id = emp.Department_Id
where emp.Chief_Id is null;
----7. SQL-������, ����� ����� ������ ����� ������� �������� ���������
select top 1 emp.Name, emp.Salary
from Employee emp
where emp.Salary < (select Max(Salary) from Employee)
order by emp.Name DESC;